<?php
/**
 * Coactual Login 
 * ---
 * @author Will Cliffe
 * @copyright GPL
 * Uses MongoDB for logins
 */
namespace Coactual\Authenticate;

use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;

class Login
{
    public function __construct($kwargs = array())
    {
        print_r($kwargs);
        $config = new Configuration();
        $config->setProxyDir($kwargs['proxy_dir']);
        $config->setProxyNamespace('Proxies');

        $config->setDefaultDB('test');
        $config->setHydratorDir($kwargs['hydrator_dir']);
        $config->setHydratorNamespace('Hydrators');
        $reader = new AnnotationReader();
        $config->setMetadataDriverImpl(new AnnotationDriver($reader, $kwargs['classes']));

        AnnotationDriver::registerAnnotationClasses();
        
        $this->dm = DocumentManager::create(new Connection(), $config);

        print_r($config);

        $this->primary_key = isset($kwargs['primary']) ? $kwargs['primary'] : 'email';
        $this->password = isset($kwargs['password']) ? $kwargs['password'] : 'password';
    }

    public function secure($username, $password)
    {
        $user = $this->dm->createQueryBuilder('User')
                    ->select($this->primary, $this->password)
                    ->field($this->primary)->equals($username)
                    ->getQuery()
                    ->getSingleResult();
        $passfield = $this->password;
        if(password_hash($password) === $user->$passfield) {
            return true;
        }

        return false;
    }

}